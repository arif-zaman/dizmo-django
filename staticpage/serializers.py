from models import Faq, StaticPage
from rest_framework import serializers


class FaqSerializer(serializers.ModelSerializer):
    class Meta:
        model = Faq
        fields = ('id', 'question', 'answer','created', 'updated')


class UpdateFaqSerializer(serializers.Serializer):
    question 	= serializers.CharField(required=False)
    answer 		= serializers.CharField(required=False)


class StaticPageSerializer(serializers.ModelSerializer):
    class Meta:
        model = StaticPage
        fields = ('id', 'attribute', 'description', 'created', 'updated')


class UpdateStaticPageSerializer(serializers.Serializer):
    attribute 	= serializers.CharField(required=False)
    description	= serializers.CharField(required=False)