from django.db import models


class Faq(models.Model):
    question	= models.TextField(unique=True)
    answer		= models.TextField()
    created     = models.DateTimeField(auto_now_add=True, auto_now=False)
    updated     = models.DateTimeField(auto_now_add=False, auto_now=True)

    class Meta:
        ordering        = ['question']


class StaticPage(models.Model):
    attribute	= models.CharField(max_length=120, unique=True)
    description	= models.TextField()
    created     = models.DateTimeField(auto_now_add=True, auto_now=False)
    updated     = models.DateTimeField(auto_now_add=False, auto_now=True)

    class Meta:
        ordering        = ['attribute']
