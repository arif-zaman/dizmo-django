from rest_framework import status
from rest_framework.decorators import api_view
from rest_framework.response import Response
from models import Faq, StaticPage
from serializers import FaqSerializer, StaticPageSerializer, UpdateFaqSerializer, UpdateStaticPageSerializer


@api_view(['GET', 'POST'])
def faq_list(request):
    """
    List all faqs, or create a new faq.
    ---
    request_serializer: FaqSerializer
    """
    if request.method == 'GET':
        faqs = Faq.objects.all()
        serializer = FaqSerializer(faqs, many=True)
        return Response(serializer.data)

    elif request.method == 'POST':
        serializer = FaqSerializer(data=request.data)

        if serializer.is_valid():
            serializer.save()
            print serializer.data
            return Response(serializer.data, status=status.HTTP_201_CREATED)

        return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)


@api_view(['GET', 'PUT', 'DELETE'])
def faq_detail(request, id):
    """
    Retrieve, update or delete a faq instance.
    ---
    request_serializer: UpdateFaqSerializer
    """
    try:
        faq = Faq.objects.get(pk=id)
    except Faq.DoesNotExist:
        return Response(status=status.HTTP_404_NOT_FOUND)

    if request.method == 'GET':
        serializer = FaqSerializer(faq)
        return Response(serializer.data)

    elif request.method == 'PUT':
        serializer = FaqSerializer(faq, partial=True, data=request.data)

        if serializer.is_valid():
            serializer.save()
            return Response(serializer.data)

        return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)

    elif request.method == 'DELETE':
        faq.delete()
        return Response(status=status.HTTP_204_NO_CONTENT)


@api_view(['GET', 'POST'])
def staticpage_list(request):
    """
    List all staticpages, or create a new staticpage.
    ---
    request_serializer: StaticPageSerializer
    """
    if request.method == 'GET':
        staticpages = StaticPage.objects.all()
        serializer = StaticPageSerializer(staticpages, many=True)
        return Response(serializer.data)

    elif request.method == 'POST':
        serializer = StaticPageSerializer(data=request.data)
        if serializer.is_valid():
            serializer.save()
            print serializer.data
            return Response(serializer.data, status=status.HTTP_201_CREATED)
        return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)


@api_view(['GET', 'PUT', 'DELETE'])
def staticpage_detail(request, id):
    """
    Retrieve, update or delete a staticpage instance.
    ---
    request_serializer: UpdateStaticPageSerializer
    """
    try:
        staticpage = StaticPage.objects.get(pk=id)
    except StaticPage.DoesNotExist:
        return Response(status=status.HTTP_404_NOT_FOUND)

    if request.method == 'GET':
        serializer = StaticPageSerializer(staticpage)
        return Response(serializer.data)

    elif request.method == 'PUT':
        serializer = StaticPageSerializer(staticpage, partial=True, data=request.data)
        if serializer.is_valid():
            serializer.save()
            return Response(serializer.data)
        return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)

    elif request.method == 'DELETE':
        staticpage.delete()
        return Response(status=status.HTTP_204_NO_CONTENT)