from django.db import models
from profiles.models import Profile


class Notification(models.Model):
    user    = models.ForeignKey(Profile, on_delete=models.CASCADE)
    body    = models.TextField()
    created = models.DateTimeField(auto_now_add=True, auto_now=False)

    class Meta:
        ordering = ['-user','-created']
