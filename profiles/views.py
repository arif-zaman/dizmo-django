import uuid
from rest_framework import status
from rest_framework.decorators import api_view
from rest_framework.response import Response
from django.contrib.auth.hashers import make_password
from models import Profile
from django.contrib.auth.models import User
from serializers import ProfileSerializer, GetUserSerializer, CreateUserSerializer, LoginSerializer
from serializers import UpdateSerializer, UpdateDocSerializer
from serializers import EmailSerializer, EmailDocSerializer
from serializers import PasswordSerializer, PasswordDocSerializer


Role = ['admin', 'employee', 'employer']


@api_view(['POST'])
def create_user(request):
    """
    create a new user.
    ---
    request_serializer: CreateUserSerializer
    """

    if request.method == 'POST':
        serializer = CreateUserSerializer(data=request.data)

        if serializer.is_valid():
            email   = serializer.validated_data['email']
            passwd  = serializer.validated_data['password']
            role = serializer.validated_data['role']
            serializer.validated_data['password'] = make_password(passwd)

            if role in Role:
                if len(passwd) <5:
                    response = {}
                    response['password'] = "password length must be at least 5"
                    return Response(response, status=status.HTTP_400_BAD_REQUEST)

                if " " in passwd:
                    response = {}
                    response['password'] = "no whitespace allowed in password"
                    return Response(response, status=status.HTTP_400_BAD_REQUEST)

                try:
                    user = User(username=email, email=email, password=make_password(passwd))
                    user.save()
                    serializer.save()
                    return Response(status=status.HTTP_201_CREATED)
                except:
                    return Response(status=status.HTTP_400_BAD_REQUEST)
            else:
                response = {}
                response['role'] = "AlLowed Role : admin, employee, employer"
                return Response(response, status=status.HTTP_400_BAD_REQUEST)
            
        return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)


@api_view(['POST'])
def login(request):
    """
    login user.
    ---
    request_serializer: LoginSerializer
    """

    if request.method == 'POST':
        if request.data['email']:
            email   = request.data['email']
        else:
            email   = ""

        if request.data["password"]:
            passwd  = request.data['password']
        else:
            passwd  = ""

        try:
            user = User.objects.get(email=email)
        except User.DoesNotExist:
            return Response(status=status.HTTP_404_NOT_FOUND)

        if(user.check_password(passwd)):
            response = {}
            response['message'] = 'Successfully Authenticated.'
            return Response(response)

        return Response(status=status.HTTP_400_BAD_REQUEST)


@api_view(['GET'])
def user_list(request, pageName, pageSize):
    """
    list all users
    """
    page,size = int(pageName), int(pageSize)
    start = (page-1)*size
    end = start+size

    if request.method == 'GET':
        users = Profile.objects.all()[start:end]
        serializer = GetUserSerializer(users, many=True)
        return Response(serializer.data)


@api_view(['GET', 'PUT', 'DELETE'])
def user_detail(request, id):
    """
    retrieve, update or delete a user instance.
    ---
    request_serializer: UpdateDocSerializer
    """
    
    try:
        profile = Profile.objects.get(pk=id)
    except Profile.DoesNotExist:
        return Response(status=status.HTTP_404_NOT_FOUND)

    if request.method == 'GET':
        serializer = ProfileSerializer(profile)
        return Response(serializer.data)

    elif request.method == 'PUT':
        serializer = UpdateSerializer(profile, partial=True, data=request.data)
        if serializer.is_valid():
            serializer.save()
            serializer = ProfileSerializer(profile)
            return Response(status=status.HTTP_204_NO_CONTENT)

        return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)

    elif request.method == 'DELETE':
        try:
            user = User.objects.get(email=profile.email)
            user.delete()
            profile.delete()
        except Profile.DoesNotExist:
            return Response(status=status.HTTP_404_NOT_FOUND)

        return Response(status=status.HTTP_204_NO_CONTENT)



@api_view(['PUT'])
def change_email(request, id):
    """
    update email of a user instance.
    ---
    request_serializer: EmailDocSerializer
    """
    
    try:
        profile = Profile.objects.get(pk=id)
    except Profile.DoesNotExist:
        return Response(status=status.HTTP_404_NOT_FOUND)

    user = User.objects.get(email=profile.email)
    serializer = EmailSerializer(profile, data=request.data)
    if serializer.is_valid():
        serializer.validated_data['hid'] = uuid.uuid4()
        serializer.validated_data['isVerified'] = False
        user.email=serializer.validated_data['email']
        user.username=serializer.validated_data['email']

        user.save()
        serializer.save()
        serializer = ProfileSerializer(profile)

        return Response(status=status.HTTP_204_NO_CONTENT)

    return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)


@api_view(['PUT'])
def change_password(request, id):
    """
    update email of a user instance.
    ---
    request_serializer: PasswordDocSerializer
    """
    
    try:
        profile = Profile.objects.get(pk=id)
    except Profile.DoesNotExist:
        return Response(status=status.HTTP_404_NOT_FOUND)

    data = request.data
    passwd1 = data['old_password']
    passwd2 = data['new_password']

    user = User.objects.get(email=profile.email)

    if not user.check_password(passwd1):
        response = {}
        response['password'] = "password doesn't match. enter correct password."
        return Response(response, status=status.HTTP_400_BAD_REQUEST)

    if len(passwd2) <5:
        response = {}
        response['password'] = "password length must be at least 5"
        return Response(response, status=status.HTTP_400_BAD_REQUEST)

    if " " in passwd2:
        response = {}
        response['password'] = "no whitespace allowed in password"
        return Response(response, status=status.HTTP_400_BAD_REQUEST)

    user.set_password(passwd2)
    try:
        user.save()
        return Response(status=status.HTTP_204_NO_CONTENT)
    except:
        return Response(status=status.HTTP_400_BAD_REQUEST)
