from models import Profile
from rest_framework import serializers
from django.contrib.auth.models import User


class ProfileSerializer(serializers.ModelSerializer):
    class Meta:
        model = Profile
        fields = ('id', 'email', 'role', 'name', 'website', 'description', 'coverphoto', 'propic', 'isVerified', 'created', 'updated')
        
        read_only_fields = ('created')
        #write_only_fields = ('password','hid',)


class GetUserSerializer(serializers.ModelSerializer):
    class Meta:
        model = Profile
        fields = ('id', 'email', 'role','isVerified')


class CreateUserSerializer(serializers.ModelSerializer):
    class Meta:
        model = Profile
        fields = ('id','email', 'password', 'role', 'created')


class LoginSerializer(serializers.ModelSerializer):
    class Meta:
        model = Profile
        fields = ('email', 'password')
        write_only_fields = ('password',)


class EmailSerializer(serializers.ModelSerializer):
    class Meta:
        model = Profile
        fields = ('hid','email','isVerified')


class EmailDocSerializer(serializers.Serializer):
    email = serializers.EmailField()


class PasswordSerializer(serializers.ModelSerializer):
    class Meta:
        model = Profile
        fields = ('password')


class PasswordDocSerializer(serializers.Serializer):
    old_password    = serializers.CharField()
    new_password    = serializers.CharField()


class UpdateSerializer(serializers.ModelSerializer):
    class Meta:
        model = Profile
        fields = ('role', 'name', 'website', 'description', 'coverphoto', 'propic', 'isVerified', 'updated')


class UpdateDocSerializer(serializers.Serializer):
    role        = serializers.CharField(required=False)
    name        = serializers.CharField(required=False)
    website     = serializers.URLField(required=False)
    description = serializers.CharField(required=False)
    coverphoto  = serializers.ImageField(required=False)
    propic      = serializers.ImageField(required=False)
    isVerified  = serializers.BooleanField(required=False)