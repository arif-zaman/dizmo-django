# -*- coding: utf-8 -*-
# Generated by Django 1.9.2 on 2016-02-28 20:58
from __future__ import unicode_literals

from django.db import migrations, models
import uuid


class Migration(migrations.Migration):

    dependencies = [
        ('profiles', '0002_auto_20160227_1248'),
    ]

    operations = [
        migrations.RemoveField(
            model_name='profile',
            name='isActive',
        ),
        migrations.AddField(
            model_name='profile',
            name='hid',
            field=models.UUIDField(default=uuid.uuid4),
        ),
    ]
