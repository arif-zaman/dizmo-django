from django.conf.urls import url
from category import views

urlpatterns = [
    url(r'^category/$', views.category_list),
    url(r'^category/(?P<id>[0-9]+)/$', views.category_detail),
    url(r'^subcategory/$', views.subcategory_list),
    url(r'^subcategory/(?P<id>[0-9]+)/$', views.subcategory_detail),
    url(r'^subscription/$', views.create_subscription),
    url(r'^subscription/user/(?P<id>\d+)/page/(?P<pageName>\d+)/size/(?P<pageSize>\d+)/$', views.user_subscription_list),
    url(r'^subscription/category/(?P<id>\d+)/page/(?P<pageName>\d+)/size/(?P<pageSize>\d+)/$', views.category_subscription_list),
    url(r'^subscription/(?P<id>[0-9]+)/$', views.subscription_detail),
]