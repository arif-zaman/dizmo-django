from django.db import models
from profiles.models import Profile


class Chat(models.Model):
    sender      = models.ForeignKey(Profile, related_name='sender', on_delete=models.CASCADE)
    receiver    = models.ForeignKey(Profile, related_name='receiver', on_delete=models.CASCADE)
    message		= models.TextField(default="")
    created     = models.DateTimeField(auto_now_add=True, auto_now=False)

    class Meta:
        ordering = ['-created']