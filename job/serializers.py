from models import Job, Application
from rest_framework import serializers


class JobSerializer(serializers.ModelSerializer):
    class Meta:
        model = Job
        fields = ('id', 'title', 'employer', 'employee', 'category', 'subcategory', 'description', 'deadline', 'completion', 'isFeatured', 'created', 'updated', 'employerRating', 'employerReview', 'employeeRating', 'employeeReview',)


class CreateJobSerializer(serializers.ModelSerializer):
    class Meta:
        model = Job
        fields = ('id', 'title', 'employer', 'category', 'subcategory', 'description', 'deadline', 'isFeatured', 'created', 'updated')


class ApplicationSerializer(serializers.ModelSerializer):
    class Meta:
        model = Application
        fields = ('id', 'user', 'job', 'created')


class DocSerializer(serializers.Serializer):
    title        	= serializers.CharField(required=False)
    employer       	= serializers.IntegerField(required=False)
    employee     	= serializers.IntegerField(required=False)
    category 		= serializers.IntegerField(required=False)
    subcategory  	= serializers.IntegerField(required=False)
    description    	= serializers.CharField(required=False)
    deadline    	= serializers.DateTimeField(required=False)
    completion     	= serializers.DateTimeField(required=False)
    isFeatured    	= serializers.BooleanField(required=False)
    employerRating 	= serializers.FloatField(required=False)
    employerReview 	= serializers.CharField(required=False)
    employeeRating 	= serializers.FloatField(required=False)
    employeeReview 	= serializers.CharField(required=False)