from rest_framework import status
from rest_framework.decorators import api_view
from rest_framework.response import Response
from models import Job, Application
from serializers import JobSerializer, CreateJobSerializer, ApplicationSerializer, DocSerializer

@api_view(['POST'])
def create_job(request):
    """
    create a new job.
    ---
    request_serializer: CreateJobSerializer
    """

    if request.method == 'POST':
        serializer = JobSerializer(data=request.data)

        if serializer.is_valid():
            serializer.save()
            return Response(serializer.data, status=status.HTTP_201_CREATED)

        return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)


@api_view(['GET'])
def job_list(request, pageName, pageSize):
    """
    list all jobs
    """

    page,size = int(pageName), int(pageSize)
    start = (page-1)*size
    end = start+size

    if request.method == 'GET':
        jobs = Job.objects.all()[start:end]
        serializer = JobSerializer(jobs, many=True)
        return Response(serializer.data)


@api_view(['GET'])
def employer_job_list(request, id, pageName, pageSize):
    """
    list all jobs of given employer
    """

    page,size = int(pageName), int(pageSize)
    start = (page-1)*size
    end = start+size

    if request.method == 'GET':
        jobs = Job.objects.filter(employer=id)[start:end]
        serializer = JobSerializer(jobs, many=True)
        return Response(serializer.data)


@api_view(['GET'])
def employee_job_list(request, id, pageName, pageSize):
    """
    list all jobs of given employee
    """

    page,size = int(pageName), int(pageSize)
    start = (page-1)*size
    end = start+size

    if request.method == 'GET':
        jobs = Job.objects.filter(employee=id)[start:end]
        serializer = JobSerializer(jobs, many=True)
        return Response(serializer.data)


@api_view(['GET'])
def category_job_list(request, id, pageName, pageSize):
    """
    list all jobs of given employee
    """

    page,size = int(pageName), int(pageSize)
    start = (page-1)*size
    end = start+size

    if request.method == 'GET':
        jobs = Job.objects.filter(category=id)[start:end]
        serializer = JobSerializer(jobs, many=True)
        return Response(serializer.data)


@api_view(['GET'])
def subcategory_job_list(request, id, pageName, pageSize):
    """
    list all jobs of given employee
    """

    page,size = int(pageName), int(pageSize)
    start = (page-1)*size
    end = start+size

    if request.method == 'GET':
        jobs = Job.objects.filter(subcategory=id)[start:end]
        serializer = JobSerializer(jobs, many=True)
        return Response(serializer.data)


@api_view(['GET', 'PUT', 'DELETE'])
def job_detail(request, id):
    """
    Retrieve, update or delete a job instance.
    ---
    request_serializer: DocSerializer
    """
    try:
        job = Job.objects.get(pk=id)
    except Job.DoesNotExist:
        return Response(status=status.HTTP_404_NOT_FOUND)

    if request.method == 'GET':
        serializer = JobSerializer(job)
        return Response(serializer.data)

    elif request.method == 'PUT':
        serializer = JobSerializer(job, partial=True, data=request.data)

        if serializer.is_valid():
            serializer.save()
            return Response(serializer.data)

        return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)

    elif request.method == 'DELETE':
        job.delete()
        return Response(status=status.HTTP_204_NO_CONTENT)


@api_view(['GET', 'POST'])
def application_list(request):
    """
    List all applications, or create a new application.
    ---
    request_serializer: ApplicationSerializer
    """
    if request.method == 'GET':
        applications = Application.objects.all()
        serializer = ApplicationSerializer(applications, many=True)
        return Response(serializer.data)

    elif request.method == 'POST':
        serializer = ApplicationSerializer(data=request.data)
        if serializer.is_valid():
            serializer.save()
            print serializer.data
            return Response(serializer.data, status=status.HTTP_201_CREATED)
        return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)


@api_view(['GET'])
def user_application_list(request, id, pageName, pageSize):
    """
    list all applications of given user
    """
    page,size = int(pageName), int(pageSize)
    start = (page-1)*size
    end = start+size

    if request.method == 'GET':
        applications = Application.objects.filter(user=id)[start:end]
        serializer = ApplicationSerializer(applications, many=True)
        return Response(serializer.data)


@api_view(['GET'])
def job_application_list(request, id, pageName, pageSize):
    """
    list all applications of given job
    """
    page,size = int(pageName), int(pageSize)
    start = (page-1)*size
    end = start+size

    if request.method == 'GET':
        applications = Application.objects.filter(job=id)[start:end]
        serializer = ApplicationSerializer(applications, many=True)
        return Response(serializer.data)


@api_view(['DELETE'])
def application_detail(request, id):
    """
    delete a application instance.
    """
    try:
        application = Application.objects.get(pk=id)
    except Application.DoesNotExist:
        return Response(status=status.HTTP_404_NOT_FOUND)

    if request.method == 'DELETE':
        application.delete()
        return Response(status=status.HTTP_204_NO_CONTENT)
