from django.conf.urls import url
from job import views

urlpatterns = [
	url(r'^job/$', views.create_job),
    url(r'^job/page/(?P<pageName>\d+)/size/(?P<pageSize>\d+)/$', views.job_list),
    url(r'^job/employer/(?P<id>\d+)/page/(?P<pageName>\d+)/size/(?P<pageSize>\d+)/$', views.employer_job_list),
    url(r'^job/employee/(?P<id>\d+)/page/(?P<pageName>\d+)/size/(?P<pageSize>\d+)/$', views.employee_job_list),
    url(r'^job/category/(?P<id>\d+)/page/(?P<pageName>\d+)/size/(?P<pageSize>\d+)/$', views.category_job_list),
    url(r'^job/subcategory/(?P<id>\d+)/page/(?P<pageName>\d+)/size/(?P<pageSize>\d+)/$', views.subcategory_job_list),
    url(r'^job/(?P<id>[0-9]+)/$', views.job_detail),
    url(r'^application/$', views.application_list),
    url(r'^application/user/(?P<id>\d+)/page/(?P<pageName>\d+)/size/(?P<pageSize>\d+)/$', views.user_application_list),
    url(r'^application/job/(?P<id>\d+)/page/(?P<pageName>\d+)/size/(?P<pageSize>\d+)/$', views.job_application_list),
    url(r'^application/(?P<id>[0-9]+)/$', views.application_detail),
]